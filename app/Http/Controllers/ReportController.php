<?php

namespace App\Http\Controllers;

use App\Http\Resources\AttendanceResource;
use App\Http\Resources\GroupCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class ReportController extends Controller
{
    public function index()
    {
        $groupsData = new GroupCollection(Auth::user()->groups);

        $attendanceData = new AttendanceResource(
            Auth::user()->groups()
            ->with(['registrations' => function($query) {
                $query->with('student')->filter(request(['search']));
            }])
            ->filter(request(['subject', 'section']))
            ->first()
        );

        return Inertia::render('Admin/Report/Index', [
            'classItems' => $groupsData,
            'attendanceData' => $attendanceData
        ]);
    }
}
