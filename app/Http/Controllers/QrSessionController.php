<?php

namespace App\Http\Controllers;

use App\Http\Resources\GroupCollection;
use App\Jobs\ClearQrSession;
use App\Services\QrService;
use chillerlan\QRCode\QRCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Inertia\Inertia;

class QrSessionController extends Controller
{
    public function index(Request $request)
    {
        $groups = new GroupCollection(Auth::user()->groups);

        return Inertia::render('Admin/QrCode/Index', [
            'classItems' => $groups
        ]);
    }

    public function store(Request $request)
    {
        $token = Str::random(25);
        
        $data = 'https://a-track-qajsx.ondigitalocean.app/qr/sub/' . $request->subject .  '/sec/' . $request->section . '/' . $token;

        $qrSession = (new QrService)->storeSession($token, $request);
    
        ClearQrSession::dispatch($qrSession)->delay(now()->addMinutes($request->duration));

        return response()->json([
            'qrCode' => (new QRCode)->render($data)
        ]);
    }
}
